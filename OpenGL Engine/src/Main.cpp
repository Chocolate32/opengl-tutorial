#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

void framebuffer_resize_callback(GLFWwindow*, int, int);

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

int main() {

#pragma region Configure GLFW

	//initialize glfw widnow!
	if (!glfwInit()) {
		std::cerr << "Failed to initialize GLFW!\n";
		glfwTerminate();
		return -1;
	}
	
	//more about glfw hint handling at https://www.glfw.org/docs/latest/window.html#window_hints
	//setting opengl version to 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//setting opengl profile to core option (core, compatibility)
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef _MAC_OS
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

#pragma endregion

	GLFWwindow* window = glfwCreateWindow(800, 600, "OpenGL Engine", NULL, NULL);

	if (!window) {
		std::cerr << "Failed to create window!\n";
		glfwTerminate();
		return -2;
	}

	glfwMakeContextCurrent(window);


	//Initialize GLAD!
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cerr << "Failed to initialize GLAD!\n";
		return -3;
	}

	//Tell OpenGL the size of the window, Lower left corner coordinates then sizes
	glViewport(0, 0, 800, 600);

	//bind the function on every window resize
	glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);


	//rendering loop
	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		// rendering commands here
		glClearColor(0.2f, .6f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glfwSwapBuffers(window);
		//checks for any input event call
		glfwPollEvents();
	}


	//properly clean-delete all of GLFW's resources
	glfwTerminate();
	return 0;
}


//resize callback function
void framebuffer_resize_callback(GLFWwindow* windowToResize, int newWidth, int newHeight) {
	glViewport(0, 0, newWidth, newHeight);
}